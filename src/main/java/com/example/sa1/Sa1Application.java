package com.example.sa1;

import com.example.sa1.model.FactualData;
import com.example.sa1.model.LeasSquaresResult;
import com.example.sa1.model.Point;
import com.example.sa1.service.LeastSquaresService;
import com.example.sa1.service.Perceptron;
import com.example.sa1.service.StockParser;
import com.example.sa1.ui.ChartRenderer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.awt.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

//@SpringBootApplication
public class Sa1Application {

    public static void main(String[] args) {
//        ApplicationContext context = SpringApplication.run(Sa1Application.class, args);
//        context.getBean(StockParser.class).parseStocks().forEach(System.out::println);
        StockParser stockParser = new StockParser();
        LeastSquaresService leastSquaresService = new LeastSquaresService();
        List<Point> points = stockParser.parseStocks().stream().map(s -> Point.builder().x(Double.valueOf(s.getTime())).y(s.getPrice()).build()).collect(Collectors.toList());
        LeasSquaresResult params = leastSquaresService.findParams(points);
        System.out.println(params);
        EventQueue.invokeLater(() -> {

            var ex = new ChartRenderer(params, points);
            ex.setVisible(true);
        });
        List<FactualData> factualData = stockParser.prepareFactualData(params);
        new Perceptron(factualData).test(FactualData.builder().x(25.0).y(3.0).build());
    }

}
