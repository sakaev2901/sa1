package com.example.sa1.service;

import com.example.sa1.model.FactualData;
import com.example.sa1.model.LeasSquaresResult;
import com.example.sa1.model.Stock;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StockParser {

    @SneakyThrows
    public List<Stock> parseStocks() {
        File file = new File("prices.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        List<Stock> stocks = new ArrayList<>();
        int i = 1;
        while ((line = bufferedReader.readLine()) != null) {
            String[] parts = line.split(",");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            stocks.add(
                    Stock.builder()
//                            .date(LocalDate.parse(parts[0] + parts[1], formatter))
                            .time(i)
                            .price(Double.parseDouble(parts[1]))
                            .build()
            );
            i++;
        }
        return stocks;
    }

    public List<FactualData> prepareFactualData(LeasSquaresResult result) {
        return parseStocks().stream()
                .map(s -> FactualData.builder().x(Double.valueOf(s.getTime())).y(s.getPrice()).result((s.getPrice() > result.getA() * Double.valueOf(s.getTime()) + result.getB()) ? 1.0 : 0.0).build())
                .collect(Collectors.toList());
    }

}
