package com.example.sa1.service;

import com.example.sa1.model.FactualData;

import java.util.List;

public class Perceptron {
    FactualData x;
    double y;
    double[] w;
    double[][] pat = {{0, 0, 0}, {0, 1, 1}, {1, 0, 1}, {1, 1, 1}};
    List<FactualData> factualDataList;

    public Perceptron(List<FactualData> factualDataList) {
        this.factualDataList = factualDataList;
        w = new double[2];
        for (int i = 0; i < 2; i++) {
            w[i] = Math.random() * 0.2 + 0.1;
//            System.out.println("Начальные значения весов");
//            System.out.println("w[" + i + "]=" + w[i]);
        }
    }

    public void cy() {
        y = 0;
        y += x.getX() * w[0];
        y += x.getY() * w[1];
        System.out.println("Взвешенная сумма входных значений");
        System.out.println(y);
        if (y > 0.5)
            y = 1;
        else
            y = 0;

    }
//
//    public void study() {
//        double gEr = 0;
//        int m = 0;
//        do {
//            gEr = 0;
//            for (int p = 0; p < pat.length; p++) {
//                x = java.util.Arrays.copyOf(pat[p], pat[p].length - 1);
//                cy();
//                double er = pat[p][2] - y;
//                gEr += Math.abs(er);
//                for (int i = 0; i < x.length; i++) {
//                    w[i] += 0.1 * er * x[i];
//                }
//            }
//            m++;
//        } while (gEr != 0);
//        System.out.println("m=" + m);
//    }

    public void study() {
        double gEr = 0;
        int m = 0;
        do {
            gEr = 0;
            for (FactualData factualData : factualDataList) {
                x = factualData;
                cy();
                double er = factualData.getResult() - y;
                gEr += Math.abs(er);
                System.out.println("qer = " + gEr);
                System.out.println();
                double k = 1;
                w[0] += k * er * x.getX();
                w[1] += k * er * x.getY();
            }
            m++;
        } while (gEr != 0);
        System.out.println("m=" + m);
    }

    public void test(FactualData checkData) {
        study();
            x = checkData;
            cy();
            System.out.println("y=" + y);
    }
}