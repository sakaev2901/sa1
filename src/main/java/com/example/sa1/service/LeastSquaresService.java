package com.example.sa1.service;

import com.example.sa1.model.LeasSquaresResult;
import com.example.sa1.model.Point;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeastSquaresService {


    public LeasSquaresResult findParams(List<Point> points) {
        int n = points.size();
        double xySum = points.stream().mapToDouble(p -> p.getY() * p.getX()).sum();
        double xSum = points.stream().mapToDouble(Point::getX).sum();
        double ySum = points.stream().mapToDouble(Point::getY).sum();
        double x2Sum = points.stream().mapToDouble(p -> p.getX() * p.getX()).sum();
        double a = (n * xySum - xSum * ySum) / (n * x2Sum - xSum * xSum);
        System.out.println(ySum);
        System.out.println(xSum);
        double b = (ySum - a * xSum) / n;
        return LeasSquaresResult.builder().a(a).b(b).build();
    }

}
